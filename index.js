

//----------------Scania Stock Market Analyst---------------------

//----------------------------------------------------------------

//Server: 


/*

Here we construct and acquire all the necessary components for running our server, 
and accessing the express and socket extensions needed to enable the functions
for the purpose of this assignment.

*/

const express = require("express");

const app = express();

const http = require("http");

const server = http.createServer(app);

const { Server } = require("socket.io");

const res = require("express/lib/response");

const io = new Server(server);

app.use(express.json());


//Server listen: 

server.listen(3000, () => {
  console.log("server has been started");
});

//----------------------------------------------------------------

// Things for stock input: 

/*

Here we define the data for our stocks and their inital value before 
being manipulated by the patch request later in the code. 

*/

let stockList = {
  data: [
    {
      stockName: "Volvo B",
      price: 120,
    },

    {
      stockName: "Investor B",
      price: 130,
    },

    {
      stockName: "Scania B",

      price: 150,
    },
  ],
};

//----------------------------------------------------------------

//Get: 

/*Here we have constructed our .get request and it's corresponding 
response of an html-file on the server, in this case the index.html.

*/

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

/* 

Here we find the json file that present the stock price data we updated 
using our patch request further down in the code.

*/

app.get("/current", (req, res) => {
  res.json(stockList);
});


//----------------------------------------------------------------

//Patch: 

/* Here we construct the patch request for updating the stock data that we defined above on line 42. 
As in the instructions of the assignment, when the url localhost.3000/update is entered, it triggers the patch request, within 
which we can update the stock prices to the corresponding stock names. 

The patch request is constructed as an "switch case statement", which reads the entered stockName and and compare it
to our array stockList we that we constructed at line 42. If the entered value of stockName in the patch request does not match 
the value stockName holds in stockList, it will go to the "else" statement. If the entered value do get a match, 
it will update the value of the entered stockName using the "object.assign". Using the io.emit from sockets.io,
we can send an update message and update the value of the stock prices on our index.html page.

*/

app.patch("/update", (req, res) => {
  const { data } = req.body;
  const stockFound = stockList.data.find(
    (s) => s["stockName"] === data["stockName"]
  );


  if (stockFound) {
    Object.assign(stockFound, data);
    res.status(200).json(stockFound);

    switch (data.stockName) {
      case "Volvo B":
        console.log(stockFound);
        io.emit("Volvo B price", {"stockName": stockFound.stockName, "price":stockFound.price});
      
        break;

      case "Investor B":
        console.log(stockFound);
        io.emit("Investor B price", {"stockName": stockFound.stockName, "price":stockFound.price});
     
        break;

      case "Scania B":
        console.log(stockFound);
        io.emit("Scania B price", {"stockName": stockFound.stockName, "price":stockFound.price});
      
        break;
    }
  } else {
    res.status(404).json({ message: "Stock does not exist" });
    console.log("Stock does not exist");
  }
});

//----------------------------------------------------------------

